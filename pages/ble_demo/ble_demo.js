var ble_master = require('../ble/ble_master.js');

// pages/ble_demo/ble_demo.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    ble_work_service_uuid: "F000FFF0-0451-4000-B000-000000000000",
    ble_work_char_uuid: "F000FFF1-0451-4000-B000-000000000000",
  },

  /**
   * 生命周期函数--监听页面加载
   */
  ble_getnotify: function(data) {
    console.log(ble_master.getStringFromBuffer(data))
  },

  afterChangeWorkUUIDs: function() {
    ble_master.ble_sendData("1234")
    // console.log(ble_master.data.service_uuid)
  },

  afterTurnOnNotify: function() {
    // ble_master.ble_sendDataByUUIDs(this.data.ble_work_service_uuid, this.data.ble_work_char_uuid, "1234")
    ble_master.ble_changeUUIDs(this.data.ble_work_service_uuid, this.data.ble_work_char_uuid, this.afterChangeWorkUUIDs)
  },

  afterConnectDevice: function() {
    ble_master.ble_getNotifyData(this.ble_getnotify)
    // ble_master.ble_turnOnNotify(this.data.ble_work_service_uuid, this.data.ble_work_char_uuid, this.afterTurnOnNotify)
    ble_master.ble_turnOnNotify(this.data.ble_work_service_uuid, this.data.ble_work_char_uuid, this.afterChangeWorkUUIDs)
  },

  disconnect_device: function() {
    ble_master.ble_disconnect()
  },

  afterStopDiscovery_Connect: function() {
    ble_master.ble_connectToDevice(this.afterConnectDevice)
  
    
  },

  foundCallback: function() {
    if (ble_master.ble_getDeviceinFoundDeviceListByName("ZCS-Dv0004")) {
      ble_master.ble_stopDiscovery(this.afterStopDiscovery_Connect)
    } 
  },

  onLoad: function (options) {
    // console.log(ble_master.getIntFromString(ble_master.getStringFromInt(567)))
    // console.log(ble_master.getStringFromBuffer(4564))
    console.log(ble_master.getStringFromBuffer(ble_master.getCharsFromInt(564)))

    ble_master.ble_onDeviceFound(this.foundCallback)
    ble_master.ble_openAdaptorandStartDiscovery()
    
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})